<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('charges', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('lease_id')->unsigned();
            $table->foreign('lease_id')->references('id')->on('leases');
            $table->date('date');
            $table->double('amount', 15, 2);
            $table->string('note')->nullable();
            
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('charges');
    }
}
