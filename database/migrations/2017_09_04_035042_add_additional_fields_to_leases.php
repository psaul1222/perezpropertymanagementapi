<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsToLeases extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('appliance_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('type');
        });
        Schema::create('appliance_manufacturers', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
        });
        Schema::create('appliance_models', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->integer('appliance_manufacturer_id')->unsigned();
            $table->foreign('appliance_manufacturer_id')->references('id')->on('appliance_manufacturers');
            $table->integer('appliance_type_id')->unsigned();
            $table->foreign('appliance_type_id')->references('id')->on('appliance_types');
        });

        Schema::create('appliances', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('aquired_date')->nullable();
            $table->double('current_value', 15, 2)->nullable();
            $table->integer('appliance_model_id')->unsigned();
            $table->foreign('appliance_model_id')->references('id')->on('appliance_models');
            $table->string('serial')->nullable();
        });

        Schema::create('lease_appliance', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('lease_id')->unsigned();
            $table->foreign('lease_id')->references('id')->on('leases');
            $table->integer('appliance_id')->nullable()->unsigned();
            $table->foreign('appliance_id')->references('id')->on('appliances');
            $table->integer('appliance_type_id')->unsigned();
            $table->foreign('appliance_type_id')->references('id')->on('appliance_types');

            $table->date('start_date');
            $table->date('end_date')->nullable();
        });

        Schema::create('system_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string("type")->unique();
            $table->string("value");
        });
        Schema::create('parking_spaces', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string("number");
            $table->integer('property_id')->unsigned();
            $table->foreign('property_id')->references('id')->on('properties');
        });
           Schema::table('tenants', function (Blueprint $table) {
            $table->string('middle_name')->nullable()->change();
            $table->integer('lease_id')->nullable()->unsigned();
            $table->foreign('lease_id')->references('id')->on('leases');
        });
        Schema::table('rental_units', function (Blueprint $table) {
            $table->string('owner_paid_utilities')->nullable();
        });
        Schema::table('leases', function (Blueprint $table) {

            $table->string('pets')->nullable();
            $table->string('storage')->nullable();
            $table->integer('max_occupancy');
            $table->double('per_additional_occupant', 15, 2);

            $table->integer('parking_space_id')->nullable()->unsigned();
            $table->foreign('parking_space_id')->references('id')->on('parking_spaces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('rental_units', function (Blueprint $table) {
            $table->dropColumn(['owner_paid_utilities']);
        });
        Schema::table('tenants', function (Blueprint $table) {

            $table->dropForeign('tenants_lease_id_foreign');
            $table->dropColumn(['lease_id']);
            $table->string('middle_name')->change();
        });
        Schema::table('leases', function (Blueprint $table) {

            $table->dropForeign('leases_parking_space_id_foreign');
            $table->dropColumn(['parking_space_id', 'pets', 'storage', 'max_occupancy', 'per_additional_occupant']);
        });

        Schema::dropIfExists('lease_appliance');
        Schema::dropIfExists('appliances');
        Schema::dropIfExists('appliance_models');
        Schema::dropIfExists('appliance_manufacturers');
        Schema::dropIfExists('appliance_types');
        Schema::dropIfExists('parking_spaces');
        Schema::dropIfExists('system_settings');
    }

}
