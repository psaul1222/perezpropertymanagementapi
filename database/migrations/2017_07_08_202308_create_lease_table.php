<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('leases', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('rental_unit_id')->unsigned();
            $table->foreign('rental_unit_id')->references('id')->on('rental_units');
            
            $table->integer('tenant_id')->unsigned();
            $table->foreign('tenant_id')->references('id')->on('tenants');
            
            $table->integer('term');
            $table->date('start_date');
            $table->double('deposit', 15, 2);
            $table->double('rent', 15, 2);
            $table->double('late_charge', 15, 2)->default(25);
            $table->double('return_check', 15, 2)->default(25);
            $table->boolean('late_charge_enabled')->default(false);
            $table->integer('late_charge_days')->default(5);
            
            
            $table->boolean('read_only')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('leases');
    }
}
