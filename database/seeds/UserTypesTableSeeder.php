<?php

use Illuminate\Database\Seeder;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('user_types')->insert([
            'title' => 'superAdmin',
        ]);
        DB::table('user_types')->insert([
            'title' => 'admin',
        ]);
        DB::table('user_types')->insert([
            'title' => 'manager',
        ]);
    }
}
