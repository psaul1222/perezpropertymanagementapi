<?php

use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        DB::table('payment_types')->insert([
            'type' => 'cash',
        ]);
        DB::table('payment_types')->insert([
            'type' => 'check',
        ]);
        DB::table('payment_types')->insert([
            'type' => 'money order',
        ]);
    }
}
