<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'name' => 'property',
        ]);
        DB::table('pages')->insert([
            'name' => 'rentalUnit',
        ]);
        DB::table('pages')->insert([
            'name' => 'tenant',
        ]);
        DB::table('pages')->insert([
            'name' => 'lease',
        ]);
        DB::table('pages')->insert([
            'name' => 'payment',
        ]);
        DB::table('pages')->insert([
            'name' => 'user',
        ]);
        DB::table('pages')->insert([
            'name' => 'admin',
        ]);
        DB::table('pages')->insert([
            'name' => 'report',
        ]);
    }
}
