<?php

use Illuminate\Database\Seeder;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'superAdmin',
            'email' => 'superAdmin',
            'password' => bcrypt('password'),
            'admin' => true,
            'manager'=> true             
        ]);
    }
}
