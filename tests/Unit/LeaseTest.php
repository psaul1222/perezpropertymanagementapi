<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LeaseTest extends TestCase
{
    /**
     * Test for get Charges sum Test on Lease Model
     *
     * 
     */
    public function testGetChargesSum()
    {
        $charges = [];
        $charges[] = new \App\Charge();
        $charges[] = new \App\Charge();
        $charges[] = new \App\Charge();
        $charges[] = new \App\Charge();
        $charges[0]->amount = 55;
        $charges[1]->amount = 45;
        $charges[2]->amount = 25;
        $charges[3]->amount = 15;
        //totals 140
        
        $lease = new \App\Lease();
        $lease->charges = $charges;
        
        
        
        $this->assertEquals(140,$lease->getChargesSum());
    }
    
     public function testGetSumClearedPayments()
    {
        $payments = [];
        $payments[] = new \App\Payment();
        $payments[] = new \App\Payment();
        $payments[] = new \App\Payment();
        $payments[] = new \App\Payment();
        $payments[0]->amount = 60;
        $payments[0]->cleared = true;
        $payments[1]->amount = 20;
        $payments[1]->cleared = true;
        $payments[2]->amount = 20;
        $payments[2]->cleared = true;
        $payments[3]->amount = 20;
        $payments[3]->cleared = true;
        //totals 140
        
        $lease = new \App\Lease();
        $lease->clearedPayments = $payments;
        
        
        
        $this->assertEquals(120,$lease->getSumClearedPayments());
    }
}
