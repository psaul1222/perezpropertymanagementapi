This is the laravel API for the perez property management application.

Perez Property Management 
	Application for storing property and tenant data.
	Saves relevant data for use in day to day running of property management.
		Data such as rental payments, contact information and lease information.
	There is a Jasper Plugin for creation of Leases and other documentation.
	