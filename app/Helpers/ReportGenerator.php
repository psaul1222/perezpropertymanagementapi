<?php
namespace App\Helpers;

use JasperPHP\JasperPHP as JasperPHP;
/**
 * class to create Jasper reports directly
 *
 * @author sperez
 */
class ReportGenerator {

    
    public static function generateRentalIncrease($apt_number,$tenant,$Premises,$one,$two,$three,$threeA,$current_rent,$changed_rent,$current_rent_due_date,
            $first_new_rent_due_date,$four,$four_line_one,$four_line_two,$four_line_three,$dated)
    {
        $jasper = new JasperPHP;

        return $jasper->process(
             base_path() . '/app/Reports/rent_increase.jrxml',
             base_path() .'/app/Documents/Leases/generated/RentIncrease_'.$apt_number,
            array("pdf"),
            array(
                 "imageFile"=>'"'.base_path() . '/app/Reports/rentalIncrease.png"',
               "apt_number" =>'"'.$apt_number.'"',
                "tenant" =>'"'.$tenant.'"',
                "Premises" =>'"'.$Premises.'"',
                "one" =>'"'.$one.'"',
                "two" =>'"'.$two.'"',
                "three" =>'"'.$three.'"',
                "threeA" =>'"'.$threeA.'"',
                "current_rent" =>'"'.$current_rent.'"',
                "changed_rent" =>'"'.$changed_rent.'"',
                "current_rent_due_date" =>'"'.$current_rent_due_date.'"',
                "first_new_rent_due_date" =>'"'.$first_new_rent_due_date.'"',
                "four" =>'"'.$four.'"',
                "four_line_one" =>'"'.$four_line_one.'"',
                "four_line_two" =>'"'.$four_line_two.'"',
                "four_line_three" =>'"'.$four_line_three.'"',
                "dated" =>'"'.$dated.'"'                
                )
                    )->execute();
    }
}
