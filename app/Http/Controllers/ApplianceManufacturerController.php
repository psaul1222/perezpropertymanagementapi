<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ApplianceManufacturerController extends Controller
{
    
    public function show($id) {

        $prop = \App\ApplianceManufacturer::find($id);
        return $prop->toJson();
    }

    public function index() {
        $applianceManufacturers = \App\ApplianceManufacturer::all();        
        
        return response()->json($applianceManufacturers);
    }

    public function store(Request $request) {
        $applianceManufacturer = $request->all();

        $rules = array(
            'name' => 'required'            
        );

        $validator = Validator::make($applianceManufacturer, $rules);

        if ($validator->fails()){
            return response()->json(['message'=>'Please fill out fields.', 'status_code'=> -1]);
        }
        if ($applianceManufacturer !== NULL ) {
            
            $findManufacturer = \App\ApplianceManufacturer::where([['name', 'like', $applianceManufacturer['name']],
                            ])->get();

                        
            if(!$findManufacturer || count($findManufacturer)<1)
            {                
                $saveApplianceManufacturer = new \App\ApplianceManufacturer();
                $saveApplianceManufacturer->name = $applianceManufacturer['name'];                
                $saveApplianceManufacturer->save();
                return response()->json(['data'=>$saveApplianceManufacturer,'message'=> 'success', 'status_code'=> 1]);
            }           
            
        }
        return response()->json(['data'=>null,'message'=> 'Manufacturer already exists!', 'status_code'=> -1]);
   
        
    }

    public function destroy($id) {
        \App\ApplianceManufacturer  ::destroy($id);
        return "Deleted " . $id;
    }

    public function update(Request $request, $id) {
        //todo 
    }
}
