<?php

namespace App\Http\Controllers;

use App\Property;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class AccessController extends Controller
{
    
    public function index() {
        $user = \Auth::user();
        $pages = ['login'];
        $data = null;
        if($user)
        {
            if($user->admin)
            {
                $pagesObj = \App\Page::all();
                foreach($pagesObj as $page)
                {
                    $page->userTypes;
                }
                $data = $pagesObj;
            }
            else
            {                
                $pagesObj = $user->userType->pages;
            }
            $pages = $pagesObj->pluck("name")->toArray();
            $pages[] = 'logout';
           
        }        
        return response()->json(['data'=>$data,'pages'=>$pages]);
    }

 
}
