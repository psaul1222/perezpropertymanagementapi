<?php

namespace App\Http\Controllers;

use App\Property;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Lease;

class LeaseController extends Controller {

    private $generatedPDFPath = '/app/Documents/Leases/generated/';

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
//    public function __construct()
//    {
//        $this->middleware('auth', ['except' => ['index','show']]);
//    }
    public function show($id) {

        $prop = \App\Lease::find($id);
        $prop->tenant;
        $prop->rentalUnit->address;
        return $prop->toJson();
    }

    public function index() {

        $leases = \App\Lease::all();
        foreach ($leases as $lease) {
            $lease->tenant;
            $lease->rentalUnit->address;
            $lease->owed = $lease->getLeaseOwed();
            $lease->currentAppliances;
            if (count($lease->currentAppliances) > 0) {
                foreach ($lease->currentAppliances as $appliance) {
                    $appliance->applianceModel;
                    if (count($appliance->applianceModel) > 0) {
                        $appliance->applianceModel->applianceManufacturer;
                    }
                }
            }
            $lease->pdf = file_exists(base_path() . $this->generatedPDFPath . 'lease_' . $lease->id . ".pdf");
        }

        return $leases->toJson();
    }

    public function store(Request $request) {
        $lease = $request->all();

        $rules = array(
            'rental_unit_id' => 'required',
            'tenant_id' => 'required',
            'term' => 'required',
            'start_date' => 'required',
            'deposit' => 'required',
            'rent' => 'required',
            'return_check' => 'required',
            'late_charge_days' => 'required',
            'late_charge' => 'required',
            'max_occupants' => 'required',
            'per_additional_occupant' => 'required'
        );

        $validator = Validator::make($lease, $rules);

        if ($validator->fails())
            return response()->json(['data' =>$lease,'message' => 'Complete Lease details are required', 'status_code' => -1]);

        if ($lease !== NULL) {

            $findLease = \App\Lease::where([
                        ['rental_unit_id', '=', $lease['rental_unit_id']],
                        ['tenant_id', '=', $lease['tenant_id']]
                    ])->get();

            if (count($findLease) > 0) {

                return "Lease Already Exists";
            }
            //todo add check to prevent multiple active leases on same rental unit.


            $findTenant = \App\Tenant::find($lease['tenant_id']);
            $findRU = \App\RentalUnit::find($lease['rental_unit_id']);

            if ($findTenant === null || $findRU === null) {
                return response()->json(['data' =>$lease,'message' => 'Could not find tenant/rental unit.', 'status_code' => -1]);
            }

            $saveLease = new \App\Lease();

            $saveLease->term = $lease['term'];

            $saveLease->start_date = $lease['start_date_formated'];
            $saveLease->deposit = $lease['deposit'];
            $saveLease->rent = $lease['rent'];
            $saveLease->late_charge = $lease['late_charge'];
            $saveLease->late_charge_days = $lease['late_charge_days'];
            $saveLease->return_check = $lease['return_check'];
            $saveLease->late_charge_enabled = false;
            $saveLease->read_only = false;
            $saveLease->max_occupancy = $lease['max_occupants'];
            $saveLease->per_additional_occupant = $lease['per_additional_occupant'];
            $saveLease->rentalUnit()->associate($findRU);
            $saveLease->tenant()->associate($findTenant);
            $saveLease->save();
            $message = $saveLease->generateLeasePDF();
            if (array_key_exists('owed', $lease) && $lease['owed']) {
                $paymentType = \App\PaymentType::where([['type', '=', 'cash']])->get()->first();
                $manager = \Auth::user();
                $payment = $saveLease->initializeLeasePayments($paymentType, $lease['owed'], $manager);
                if ($payment->amount > 0) {
                    $payment->save();
                }
            }
            if (array_key_exists('appliance_types', $lease) && $lease['appliance_types']) {
                foreach ($lease['appliance_types'] as $appl) {
                    $findApplianceType = \App\ApplianceType::find($appl['id']);
                    $saveLease->appliances()->attach($findApplianceType->id, ['start_date' => $lease['start_date_formated']]);
                }
            }

            return response()->json(['data' => $saveLease, 'message' => $message, 'status_code' => 1]);
        }
        return response()->json(['data' =>$lease,'message' => 'General Error!', 'status_code' => -1]);
    }

    public function destroy($id) {
        \App\Lease::destroy($id);
        $leaseFileName = base_path() . $this->generatedPDFPath . 'lease_' . $id . ".pdf";
        $message = "Deleted $id ";
        if (file_exists($leaseFileName)) {
            unlink($leaseFileName);
            $message .= "and lease_" . $id . ".pdf";
        }
        return response()->json(['data' => null, 'message' => $message, 'status_code' => 1]);
    }

    public function update(Request $request, $id) {

        $tenant = $request->all();

        if (count($tenant) == 0)
            return "no changes made";

        //add in later

        return 'success';
    }

    public function getPdf($id) {
        $pdf = base_path() . $this->generatedPDFPath . 'lease_' . $id . ".pdf";


        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($pdf, 'lease_' . $id . '.pdf', $headers);
    }

}
