<?php

namespace App\Http\Controllers;

use App\Property;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PaymentController extends Controller
{
    
    public function show($id) {

        $prop = \App\Payment::find($id);
        $prop->paymentType;
        $prop->lease;
        $prop->manager;
        return $prop->toJson();
    }

    public function index() {
        $payments = \App\Payment::all();
        foreach($payments as $payment)
        {
            $payment->manager;
            $payment->lease->rentalUnit->address;
            $payment->lease->tenant;
            $payment->paymentType;
        }
        
        return response()->json($payments);
    }

    public function store(Request $request) {
        $payment = $request->all();

        $rules = array(
            'lease' => 'required',
            'paymentType' => 'required',
            'amount' => 'required',
            'date' => 'required'
        );

        $validator = Validator::make($payment, $rules);

        if ($validator->fails()){
            return response()->json(['message'=>'Please fill out fields.', 'status_code'=> -1]);
        }
        
        if ($payment !== NULL ) {

            $findLease = \App\Lease::find($payment['lease']['id']);
            $findPaymentType = \App\PaymentType::find($payment['paymentType']['id']);
            $manager = \Auth::user();
            
            if($findLease && $findPaymentType && $manager)
            {                
                $savePayment = new \App\Payment();
                $savePayment->date = $payment['date'];
                $savePayment->amount = $payment['amount'];
                $savePayment->lease()->associate($findLease);
                $savePayment->paymentType()->associate($findPaymentType);
                $savePayment->manager()->associate($manager);
                $savePayment->save();
                return response()->json(['data'=>$savePayment,'message'=> 'sucess', 'status_code'=> 1]);
            }           
            
        }
        return response()->json(['data'=>$saveTenant,'Error!'=> 'sucess', 'status_code'=> -1]);
    }

    public function destroy($id) {
        \App\Payment::destroy($id);
        return "Deleted " . $id;
    }

    public function clear(Request $request,$id) {
        $payment = $request->all();         
        $updatePayment = \App\Payment::find($payment['id']);
        $updatePayment->cleared = $payment['cleared'];
        $updatePayment->save();
    }
    public function update(Request $request) {
        $payment = $request->all();         
        $updatePayment = \App\Payment::find($payment['id']);
        $updatePayment->cleared = $payment['cleared'];
        //$updatePayment->save();
    }
}
