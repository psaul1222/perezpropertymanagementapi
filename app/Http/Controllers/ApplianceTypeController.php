<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ApplianceTypeController extends Controller
{
    
    public function show($id) {

        $prop = \App\ApplianceType::find($id);
        return $prop->toJson();
    }

    public function index() {
        $applianceTypes = \App\ApplianceType::all();        
        
        return response()->json($applianceTypes);
    }

    public function store(Request $request) {
        $applianceType = $request->all();

        $rules = array(
            'type' => 'required'            
        );

        $validator = Validator::make($applianceType, $rules);

        if ($validator->fails()){
            return response()->json(['message'=>'Please fill out fields.', 'status_code'=> -1]);
        }
        if ($applianceType !== NULL ) {
            
            $findType = \App\ApplianceType::where([['type', 'like', $applianceType['type']],
                            ])->get();

                        
            if(!$findType || count($findType)<1)
            {                
                $saveApplianceType = new \App\ApplianceType();
                $saveApplianceType->type = $applianceType['type'];                
                $saveApplianceType->save();
                return response()->json(['data'=>$saveApplianceType,'message'=> 'success', 'status_code'=> 1]);
            }           
            
        }
        return response()->json(['data'=>$saveApplianceType,'Error!'=> 'success', 'status_code'=> -1]);
   
        
    }

    public function destroy($id) {
        \App\ApplianceType  ::destroy($id);
        return "Deleted " . $id;
    }

    public function update(Request $request, $id) {
        //todo 
    }
}
