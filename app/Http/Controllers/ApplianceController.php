<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ApplianceController extends Controller
{

    public function show($id) {

        $app = \App\Appliance::find($id);
        $app->applianceModel;
        $app->currentLease->rentalUnit->address;
        return $app->toJson();
    }

    public function index() {
        $apps = \App\Appliance::all();
        foreach($apps as $app)
        {
            $app->applianceModel->applianceManufacturer;
            $app->applianceModel->applianceType;
            $app->currentLease;
            if(count($app->currentLease)>0)
            {
                $app->currentLease->rentalUnit->address;
            }
        }

        return response()->json($apps);
    }

    public function store(Request $request) {
        $appliance = $request->all();

        $rules = array(
            'model' => 'required'
        );

        $validator = Validator::make($appliance, $rules);

        if ($validator->fails()){
            return response()->json(['message'=>'Please fill out fields.', 'status_code'=> -1]);
        }

        if ($appliance !== NULL ) {

            $findModel = \App\ApplianceModel::find($appliance['model']['id']);

            if($findModel)
            {
                $saveAppliance = new \App\Appliance();
                if(array_key_exists("serial", $appliance))
                {
                    $saveAppliance->serial = $appliance['serial'];
                }

                //$saveAppliance->currentLease()->attach($findLease->id,['start_date'=>$appliance['start_date']]);

                $saveAppliance->applianceModel()->associate($findModel);

                $saveAppliance->save();
                $saveAppliance->applianceModel->applianceManufacturer;
                $saveAppliance->applianceModel->applianceType;
                return response()->json(['data'=>$saveAppliance,'message'=> 'success', 'status_code'=> 1]);
            }

        }
        return response()->json(['data'=>null,'message'=> 'Missing fields!', 'status_code'=> -1]);
    }

    public function destroy($id) {
        \App\Appliance::destroy($id);
        return "Deleted " . $id;
    }
}
