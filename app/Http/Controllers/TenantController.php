<?php

namespace App\Http\Controllers;

use App\Property;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TenantController extends Controller {


    public function show($id) {

        $prop = \App\Tenant::find($id);
        $prop->address;
        return $prop->toJson();
    }

    public function index() {
        return response()->json(\App\Tenant::all());
    }

    public function store(Request $request) {
        $tenant = $request->all();

        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required'
        );

        $validator = Validator::make($tenant, $rules);

        if ($validator->fails())
            return "Full name is required";

        if ($tenant !== NULL ) {

            
            if(array_key_exists("id_number", $rules))
            {
                $findTenant = \App\Tenant::where([['identification_number', 'like', $tenant['id_number']]])->get()->first();
            }
            else
            {
                $findTenant = \App\Tenant::where([['first_name', 'like', $tenant['first_name']],
                            ['last_name', 'like', $tenant['last_name']]])->get()->first();
            }
            if (count($findTenant) > 0) {
              
                    return "Rental Unit Already Exists";
                
            }

            $saveTenant = new \App\Tenant();

            if (array_key_exists('middle_name', $tenant) && $tenant['middle_name'] !== "") {
                $name = $tenant['middle_name'];
            } else {
                $name ="";
            }
            $saveTenant->first_name = $tenant['first_name'];
            $saveTenant->middle_name = $name;
            $saveTenant->last_name = $tenant['last_name'];
            
            if (array_key_exists('identification_number', $tenant) && $tenant['id_number'] !== "") {
                $saveTenant->identification_number = $tenant['id_number'];
            }
            
            
            $saveTenant->save();
            return response()->json(['data'=>$saveTenant,'message'=> 'sucess', 'status_code'=> 1]);
        }
        return "error";
    }

    public function destroy($id) {
        \App\Tenant::destroy($id);
        return "Deleted " . $id;
    }

    public function update(Request $request, $id) {

        $tenant = $request->all();

        if (count($tenant) == 0)
            return "no changes made";

        $updateTenant = \App\Tenant::find($id);
        if (array_key_exists("first_name", $tenant))
            $updateTenant->first_name = $tenant['first_name'];

        if (array_key_exists("last_name", $tenant))
            $updateTenant->last_name = $tenant['last_name'];
        
        if (array_key_exists("middle_name", $tenant))
            $updateTenant->middle_name = $tenant['middle_name'];
        
        if (array_key_exists("id_number", $tenant))
            $updateTenant->identification_number = $tenant['id_number'];

        $updateTenant->save();

        return 'success';
    }

}
