<?php

namespace App\Http\Controllers;

use App\Property;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PropertyController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
//    public function __construct()
//    {
//        $this->middleware('auth', ['except' => ['index','show']]);
//    }
    public function show($id) {

        $prop = \App\Property::find($id);
        $prop->address;
        return $prop->toJson();
    }

    public function index() {
        $properties = \App\Property::all();
        foreach($properties as $property)
        {
            $property->address;
            $property->rentalUnits;
        }
        
        return $properties->toJson();
    }

    public function store(Request $request) {
        $property = $request->all();
        
         $rules = array(
            'street'       => 'required',
            'city'       => 'required',
            'state'       => 'required',
            'zip'       => 'required'
           
        );
        if(array_key_exists('address', $property))
        {
            $validator = Validator::make($property['address'], $rules);
        
            if($validator->fails()) return "Full Address is required";
        }
        else
        {
            return response()->json(['message'=> 'bad form data', 'status_code'=> -1]);
        }
        
        if ($property != NULL && $property['address'] != NULL) {

            //check if address already exists
            if (array_key_exists('apartment', $property['address'])) {
                $findAddress = \App\Address::where([['apartment', 'like', $property['address']['apartment']], ['street', 'like', $property['address']['street']],
                            ['city', 'like', $property['address']['city']], ['state', 'like', $property['address']['state']], ['zip', 'like', $property['address']['zip']]])->get()->first();
            } else {
                $findAddress = \App\Address::where([['street', 'like', $property['address']['street']],
                            ['city', 'like', $property['address']['city']], ['state', 'like', $property['address']['state']], ['zip', 'like', $property['address']['zip']]])->get()->first();
            }
            if (count($findAddress) > 0) {
                // check if property already exists with address

                $propertyFound = \App\Property::where('main_address_id', '=', $findAddress->id)->get()->first();
                if (count($propertyFound) > 0) {                    
                    return response()->json(['message'=> 'Property Already Exists', 'status_code'=> -1]);
                }
            } else {
                $findAddress = new \App\Address();
                $findAddress->street = $property['address']['street'];
                $findAddress->city = $property['address']['city'];
                $findAddress->state = $property['address']['state'];
                $findAddress->zip = $property['address']['zip'];
                if (array_key_exists('apartment', $property['address']))
                    $findAddress->apartment = $property['address']['apartment'];

                $findAddress->save();
            }

            $saveProperty = new \App\Property();
            
            if (array_key_exists('name', $property) && $property['name'] !=="")
            {
                $name = $property['name'];
            }
            else
            {
                $name = $findAddress->street." ".$findAddress->city;
            }
            $saveProperty->name = $name;
            
            
            
            $saveProperty->address()->associate($findAddress);
            $saveProperty->save();
            return response()->json(['message'=> 'Successfully added', 'status_code'=> 1]);
        }
        return response()->json(['message'=> 'Unknown Error', 'status_code'=> -1]);
    }

    public function destroy($id) {
        \App\Property::destroy($id);
        return "Deleted " . $id;
    }

    public function update(Request $request, $id) {

        $property = $request->all();

        if (count($property) == 0)
            return "no changes made";

//        $rules = array(
//            'name'       => 'required',
//           
//        );        
//        
//        $validator = Validator::make($property, $rules);
//        
//        if($validator->fails()) return "name is required";

        $updateProperty = \App\Property::find($id);
        if (array_key_exists("name", $property))
            $updateProperty->name = $property['name'];

        if (array_key_exists("address", $property)) {
            //update address
            $updateAddress = $updateProperty->address;
            $address = $property['address'];

            if (array_key_exists('street', $address))
                $updateAddress->street = $address['street'];
            if (array_key_exists('cotu', $address))
                $updateAddress->city = $address['city'];
            if (array_key_exists('state', $address))
                $updateAddress->state = $address['state'];
            if (array_key_exists('zip', $address))
                $updateAddress->zip = $address['zip'];
            if (array_key_exists('apartment', $address))
                $updateAddress->apartment = $address['apartment'];

            $updateAddress->save();
        }
        $updateProperty->save();

        return 'success';
    }

}
