<?php

namespace App\Http\Controllers;

use App\Property;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class RentalUnitController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
//    public function __construct()
//    {
//        $this->middleware('auth', ['except' => ['index','show']]);
//    }
    public function show($id) {

        $prop = \App\RentalUnit::find($id);
        $prop->address;
        $prop->property;
        return $prop->toJson();
    }

    public function index() {
        
        
        $rentalUnits = \App\RentalUnit::all();
        foreach($rentalUnits as $ru)
        {
            $ru->address;
            $ru->property;
        }
        
        return $rentalUnits->toJson();       
        
    }

    public function store(Request $request) {
        $rentalUnit = $request->all();
        
         $rules = array(
            'street'       => 'required',
            'city'       => 'required',
            'state'       => 'required',
            'zip'       => 'required'
           
        );
        if(array_key_exists('address', $rentalUnit))
        {
            $validator = Validator::make($rentalUnit['address'], $rules);
        
            if($validator->fails()) return response()->json(['message'=> 'Full address required.', 'status_code'=> -1]);
            
            if(!array_key_exists('property_id', $rentalUnit))
            {                
                return response()->json(['message'=> 'A rental unit must belong to a property.', 'status_code'=> -1]);
            }
            
            
        }
        else
        {
            return response()->json(['message'=> 'Bad Form Data', 'status_code'=> -1]);
        }
        
        if ($rentalUnit != NULL && $rentalUnit['address'] != NULL) {

            //check if address already exists
            if (array_key_exists('apartment', $rentalUnit['address']) && $rentalUnit['address']['apartment']!==NULL) {
                $findAddress = \App\Address::where([['apartment', 'like', $rentalUnit['address']['apartment']], ['street', 'like', $rentalUnit['address']['street']],
                            ['city', 'like', $rentalUnit['address']['city']], ['state', 'like', $rentalUnit['address']['state']], ['zip', 'like', $rentalUnit['address']['zip']]])->get()->first();
            } else {
                $findAddress = \App\Address::where([['street', 'like', $rentalUnit['address']['street']],
                            ['city', 'like', $rentalUnit['address']['city']], ['state', 'like', $rentalUnit['address']['state']], ['zip', 'like', $rentalUnit['address']['zip']]])->get()->first();
            }
            if (count($findAddress) > 0) {
                // check if rental unit already exists with address

                $ruFound = \App\RentalUnit::where('address_id', '=', $findAddress->id)->get()->first();
                if (count($ruFound) > 0) {
                    return response()->json(['message'=> 'Already exists.', 'status_code'=> -1]);
                }
            } else {
                $findAddress = new \App\Address();
                $findAddress->street = $rentalUnit['address']['street'];
                $findAddress->city = $rentalUnit['address']['city'];
                $findAddress->state = $rentalUnit['address']['state'];
                $findAddress->zip = $rentalUnit['address']['zip'];
                if (array_key_exists('apartment', $rentalUnit['address'])  && $rentalUnit['address']['apartment']!==NULL )
                    $findAddress->apartment = $rentalUnit['address']['apartment'];

                $findAddress->save();
            }
            
            $propertyFound = \App\Property::find($rentalUnit['property_id']);
            
            if(!$propertyFound)
            {
                return response()->json(['message'=> 'Invalid property.', 'status_code'=> -1]); 
            }
            
            
            
            $saveRU = new \App\RentalUnit();
            
            if (array_key_exists('name', $rentalUnit) && $rentalUnit['name'] !=="")
            {
                $name = $rentalUnit['name'];
            }
            else
            {
                $name = $findAddress->street." ".$findAddress->city;
            }
            $saveRU->name = $name;
            
            
            
            $saveRU->address()->associate($findAddress);
            $saveRU->property()->associate($propertyFound);
            $saveRU->save();
            return response()->json(['data'=>$saveRU,'message'=> 'Successfully added', 'status_code'=> 1]);
        }
        return "error";
    }

    public function destroy($id) {
        \App\RentalUnit::destroy($id);
        return "Deleted " . $id;
    }

    public function update(Request $request, $id) {

        $rentalUnit = $request->all();

        if (count($rentalUnit) == 0)
            return "no changes made";

//        $rules = array(
//            'name'       => 'required',
//           
//        );        
//        
//        $validator = Validator::make($property, $rules);
//        
//        if($validator->fails()) return "name is required";

        $updateRU = \App\RentalUnit::find($id);
        if (array_key_exists("name", $rentalUnit))
            $updateRU->name = $rentalUnit['name'];

        if (array_key_exists("address", $rentalUnit)) {
            //update address
            $updateAddress = $updateRU->address;
            $address = $rentalUnit['address'];

            if (array_key_exists('street', $address))
                $updateAddress->street = $address['street'];
            if (array_key_exists('cotu', $address))
                $updateAddress->city = $address['city'];
            if (array_key_exists('state', $address))
                $updateAddress->state = $address['state'];
            if (array_key_exists('zip', $address))
                $updateAddress->zip = $address['zip'];
            if (array_key_exists('apartment', $address))
                $updateAddress->apartment = $address['apartment'];

            $updateAddress->save();
        }
        $updateRU->save();

        return response()->json(['message'=> 'Successfully added', 'status_code'=> 1]);
    }

}
