<?php

namespace App\Http\Controllers;

use App\Property;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class SystemSettingController extends Controller {

    public function show($id) {

        $prop = \App\SystemSetting::find($id);
        return $prop->toJson();
    }

    public function index() {

        $settings = \App\SystemSetting::all();
        return $settings->toJson();
    }

    public function store(Request $request) {
        $setting = $request->all();

        $rules = array(
            'type' => 'required',
            'value' => 'required'
        );

        $validator = Validator::make($setting, $rules);

        if ($validator->fails())
            return response()->json(['data' => $setting, 'message' => 'Fill in the fields.', 'status_code' => -1]);



        try {

            if ($setting !== NULL) {

                $findSetting = \App\SystemSetting::where([
                            ['type', '=', $setting['type']]])->get();
                if (count($findSetting) > 0) {
                    return response()->json(['data' => $setting, 'message' => "System setting already exists!", 'status_code' => -1]);
                }

                $saveSetting = new \App\SystemSetting();
                $saveSetting->type = $setting['type'];
                $saveSetting->value = $setting['value'];
                $saveSetting->save();
                return response()->json(['data' => $saveSetting, 'message' => "", 'status_code' => 1]);
            }
        } catch (Exception $ex) {
            return response()->json(['data' => $setting, 'message' => $ex, 'status_code' => -1]);
        }
    }

    public function update(Request $request) {
        $setting = $request->all();

        $rules = array(
            'type' => 'required',
            'value' => 'required'
        );

        $validator = Validator::make($setting, $rules);

        if ($validator->fails())
            return response()->json(['data' => $setting, 'message' => 'Fill in the fields.', 'status_code' => -1]);



        try {

            if ($setting !== NULL) {

                $saveSetting = \App\SystemSetting::find($setting['id']);
                $saveSetting->type = $setting['type'];
                $saveSetting->value = $setting['value'];
                $saveSetting->save();
                return response()->json(['data' => $saveSetting, 'message' => "", 'status_code' => 1]);
            }
        } catch (Exception $ex) {
            return response()->json(['data' => $setting, 'message' => $ex, 'status_code' => -1]);
        }
    }

    public function destroy($id) {
        \App\SystemSetting::destroy($id);
        
        return response()->json(['data' => null, 'message' => "Deleted ".$id, 'status_code' => 1]);
    }

}
