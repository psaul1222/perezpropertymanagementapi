<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Helpers\ReportGenerator;

class ReportController extends Controller {

    private $generatedPDFPath = '/app/Documents/Leases/generated/';

    public function generateRentIncrease(Request $request) {
        $report = $request->all();

        $rules = array(
            "apt_number" => 'required',
            "tenant" => 'required',
            "Premises" => 'required',
            "threeA" => 'required',
            "current_rent" => 'required',
            "changed_rent" => 'required',
            "current_rent_due_date" => 'required',
            "first_new_rent_due_date" => 'required',
            "dated" => 'required'
        );
        $validator = Validator::make($report, $rules);

        if ($validator->fails())
            return response()->json(['message' => 'All report fields required.', 'status_code' => -1]);
        
        $possibleEmpty = ['one','two','three','four','four_line_one','four_line_two','four_line_three'];
        foreach($possibleEmpty as $value)
        {
            if(!array_key_exists($value, $report)) $report[$value] = "";
        }

        $response = ReportGenerator::generateRentalIncrease($report['apt_number'],
            $report['tenant'],
            $report['Premises'],
            $report['one'],
            $report['two'],
            $report['three'],
            $report['threeA'],
            $report['current_rent'],
            $report['changed_rent'],
            $report['current_rent_due_date'],
            $report['first_new_rent_due_date'],
            $report['four'],
            $report['four_line_one'],
            $report['four_line_two'],
            $report['four_line_three'],
            $report['dated']);
        return ReportController::getPdf($report['apt_number']);
        
        
    }

    public function getPdf($apt_number) {
        $pdf = base_path() . $this->generatedPDFPath . 'RentIncrease_' . $apt_number . ".pdf";


        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($pdf, 'rent_increase_' . $apt_number . '.pdf', $headers);
    }

}
