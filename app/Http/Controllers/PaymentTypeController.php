<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PaymentTypeController extends Controller
{
    
    public function show($id) {

        $prop = \App\PaymentType::find($id);
        return $prop->toJson();
    }

    public function index() {
        $payments = \App\PaymentType::all();        
        
        return response()->json($payments);
    }

    public function store(Request $request) {
        
    }

    public function destroy($id) {
        \App\PaymentType::destroy($id);
        return "Deleted " . $id;
    }

    public function update(Request $request, $id) {
        //todo 
    }
}
