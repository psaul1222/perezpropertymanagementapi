<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class UserController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function auth(Request $request) {
    
        $params = $request->only('email', 'password');
    
        $username = $params['email'];
        $password = $params['password'];
    
        if (\Auth::attempt(['email' => $username, 'password' => $password])) {
            $user = \Auth::user();
            if($user->manager || $user->admin)
            {
                $token = $user->createToken('perezTMS_DCLIENT')->accessToken;
                return response()->json(['user'=>$user,'token'=>$token]);
            }
            else{
                return response()->json(['message' => 'User not authorized.', 'status_code' => -1]);
            }
            
    }

        return response()->json(['error' => 'Invalid username or Password']);
    }

        public function index() {
        return response()->json(\App\User::all());
    }
        
    public function store(Request $request) {
        $user = $request->all();

         $rules = array(
             'name' => 'required',
            'email'       => 'required',
            'password'       => 'required'
        );

        $validator = Validator::make($user, $rules);

        if($validator->fails()){
            return response()->json(['message' => 'bad form data', 'status_code' => -1]);
        }




            // check if user already exists
            $userFound = \App\User::where('email', '=', $user['email'])->get();
        if (count($userFound) > 0) {
            return response()->json(['message'=> 'User Already Exists', 'status_code'=> -1]);
        }

            $saveUser = new \App\User();

            $saveUser->name = $user['name'];
            $saveUser->email = $user['email'];
            $saveUser->setPasswordAttribute($user['password']);

            $saveUser->manager = (array_key_exists('manager', $user) && $user['manager']!== NULL) ? $user['manager'] : false;
            $saveUser->admin = (array_key_exists('admin', $user) && $user['admin']!== NULL) ? $user['admin'] : false;

            $saveUser->save();
            return response()->json(['message'=> 'Successfully added', 'status_code'=> 1]);

    }

    public function destroy($id) {
        //prevent user from deleting superAdmin
        $superAdmin = \App\User::where('name', '=', 'superAdmin')->get()->first();
        if($superAdmin && $superAdmin->id == $id)
        {
            return response()->json(['data'=>$superAdmin,'message'=> 'Cannot delete default user.', 'status_code'=> -1]);
        }
        \App\User::destroy($id);
        return "Deleted " . $id;
    }

    public function update(Request $request, $id) {

        $user = $request->all();

        if (count($user) == 0)
            return "no changes made";

    }

}
