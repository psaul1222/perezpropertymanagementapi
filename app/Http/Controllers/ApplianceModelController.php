<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ApplianceModelController extends Controller
{

    public function show($id) {

        $prop = \App\ApplianceModel::find($id);
        return $prop->toJson();
    }

    public function index() {
        $applianceModels = \App\ApplianceModel::all();
        foreach($applianceModels as $app)
        {
            $app->applianceType;
            $app->applianceManufacturer;
        }

        return response()->json($applianceModels);
    }

    public function store(Request $request) {
        $applianceModel = $request->all();

        $rules = array(
            'name' => 'required',
            'type' => 'required',
            'manufacturer' => 'required'
        );

        $validator = Validator::make($applianceModel, $rules);

        if ($validator->fails()){
            return response()->json(['message'=>'Please fill out fields.', 'status_code'=> -1]);
        }
        $findApplianceType = \App\ApplianceType::find($applianceModel['type']['id']);
        $findApplianceManufacturer = \App\ApplianceManufacturer::find($applianceModel['manufacturer']['id']);
        if ($applianceModel && $findApplianceType && $findApplianceManufacturer) {

            $findModel = \App\ApplianceModel::where([['name', 'like', $applianceModel['name']],
                            ])->get();


            if(!$findModel || count($findModel)<1)
            {
                $saveApplianceModel = new \App\ApplianceModel();
                $saveApplianceModel->name = $applianceModel['name'];
                $saveApplianceModel->applianceType()->associate($findApplianceType);
                $saveApplianceModel->applianceManufacturer()->associate($findApplianceManufacturer);
                $saveApplianceModel->save();
                return response()->json(['data'=>$saveApplianceModel,'message'=> 'success', 'status_code'=> 1]);
            }

        }
        return response()->json(['data'=>null,'message'=> 'Failed to save.', 'status_code'=> -1]);

    }

    public function destroy($id) {
        \App\ApplianceModel  ::destroy($id);
        return "Deleted " . $id;
    }

    public function update(Request $request, $id) {
        //todo
    }
}
