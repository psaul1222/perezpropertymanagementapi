<?php

namespace App\Http\Middleware;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();
            if(!$user->admin)
            {
                return response()->json(['message' => 'User not authorized.', 'status_code' => -1]);
            } 
         return $next($request);
    }
}
