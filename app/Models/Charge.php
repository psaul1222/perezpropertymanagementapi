<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    
    protected $table = 'charges';
    
    public function lease()
    {
        return $this->belongsTo('App\Lease');
    }
    
}
