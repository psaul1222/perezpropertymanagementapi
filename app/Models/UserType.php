<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    
    protected $table = 'user_types';
    
    public function pages()
    {
        return $this->belongsToMany("App\Page","page_user_type");
    }
}
