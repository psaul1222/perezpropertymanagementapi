<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';
    protected $visible =['street','apartment','city','state','zip'];    
    
}