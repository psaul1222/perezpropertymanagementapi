<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class RentalUnit extends Model
{
    
    protected $table = 'rental_units';
    public function property()
    {
        return $this->belongsTo('App\Property');
    }
    public function address()
    {
        return $this->belongsTo('App\Address');
    }
    
}
