<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ApplianceManufacturer extends Model
{
    
    protected $table = 'appliance_manufacturers';
    
}
