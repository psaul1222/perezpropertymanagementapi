<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';
    protected $visible =['id','name','address'];
    public function rentalUnits()
    {
        return $this->hasMany('App\RentalUnit');
    }
    public function address()
    {
        return $this->belongsTo('App\Address','main_address_id');
    }
    public function parkingSpaces()
    {
        return $this->hasMany('App\ParkingSpace');
    }
}
