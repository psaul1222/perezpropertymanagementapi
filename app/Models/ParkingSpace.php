<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ParkingSpace extends Model
{
    protected $table = 'parking_spaces';
    
    public function property()
    {
        return $this->belongsTo("App\Property");
    }    
}