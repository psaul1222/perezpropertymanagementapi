<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ApplianceType extends Model
{
    
    protected $table = 'appliance_types';
    
}
