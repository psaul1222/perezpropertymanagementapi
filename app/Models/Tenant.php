<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    
    protected $table = 'tenants';    
    public function address()
    {
        return $this->belongsTo('App\Address','prev_address_id');
    }
    
    public function lease()
    {
        return $this->hasOne('App\Lease');
    }
}
