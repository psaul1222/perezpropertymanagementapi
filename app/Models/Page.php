<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{    
    protected $table = 'pages';
    
    
    public function userTypes()
    {
        return $this->belongsToMany("App\UserType","page_user_type");
    }
}
