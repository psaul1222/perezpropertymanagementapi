<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    
    protected $table = 'payments';
    
    public function paymentType()
    {
        return $this->belongsTo('App\PaymentType','payment_type');
    }
    public function lease()
    {
        return $this->belongsTo('App\Lease');
    }
    public function manager()
    {
        return $this->belongsTo('App\User','manager_id');
    }
}
