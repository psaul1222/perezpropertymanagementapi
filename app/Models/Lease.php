<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use JasperPHP\JasperPHP as JasperPHP;
class Lease extends Model
{

    protected $table = 'leases';

    public function tenant()
    {
        return $this->belongsTo("App\Tenant");
    }
    public function rentalUnit()
    {
        return $this->belongsTo("App\RentalUnit");
    }
    public function charges()
    {
        return $this->hasMany('App\Charge');
    }
    public function parkingSpace()
    {
        return $this->belongsTo("App\ParkingSpace");
    }  
    public function clearedPayments()
    {
        return $this->hasMany('App\Payment')->where('cleared',true);
    }
    public function unclearedPayments()
    {
        return $this->hasMany('App\Payment')->where('cleared',false);
    }
     public function appliances()
    {
        return $this->belongsToMany("App\ApplianceType","lease_appliance");
    }
    public function currentAppliances()
    {
        return $this->belongsToMany("App\ApplianceType","lease_appliance")->where('end_date',null);
    }

    public function getChargesSum()
    {
        $sum = 0;
        foreach($this->charges as $charge)
        {
            $sum += $charge->amount;
        }
        return $sum;
    }

    public function getSumClearedPayments()
    {
        $sum = 0;
        foreach($this->clearedPayments as $payment)
        {
            $sum += $payment->amount;
        }
        return $sum;
    }
    //calculates a lease's commitment to date provided
    public function getLeaseCommitment($date = null)
    {
        if(!isset($date))
        {
            $date = new \DateTime();
        }
        $interval = $date->diff(new \DateTime($this->start_date));
        return $interval->format('%m')*$this->rent;
    }

    public function getLeaseOwed($date = null)
    {
        if(!isset($date))
        {
            $date = new \DateTime();
        }
        $amountOwed = $this->getLeaseCommitment($date) + $this->getChargesSum();
        return number_format((float)($amountOwed - $this->getSumClearedPayments() ), 2, '.', '') ;

    }

    // used for initializing an active lease.
    public function initializeLeasePayments(\App\PaymentType $paymentType,$currentlyOwed,\App\User $manager)
    {

         $payment = new \App\Payment();

         $payment->note = "Initializing lease payments";

         //temporary set manager_id to -1 until auth is setup

         $payment->manager()->associate($manager);

         $payment->cleared = 1;
         $payment->date = new \DateTime();

         $payment->lease()->associate($this);
         $payment->paymentType()->associate($paymentType);

         $payment->amount = $this->getLeaseOwed($lease) - $currentlyOwed;

         return $payment;

    }
    public function generateLeasePDF()
    {
        $jasper = new JasperPHP;

            // Compile a JRXML to Jasper
       // $jasper->compile( base_path()  . '/app/Reports/hello_world.jrxml')->execute();

            // Process a Jasper file to PDF and RTF (you can use directly the .jrxml)
        $apt = $this->rentalUnit->address->apartment;
        if(!$apt) {
            // apt is null so use street address before direction
            
            
            $lowerCaseApt = strtolower($this->rentalUnit->address->street);
            $direction = ['west','south','w','s','north','east','n','e'];
            $j =0;
            $splitOnDirection = [];
            while(count($splitOnDirection) <=1 && $j < count($direction))
            {
                $splitOnDirection = explode($direction[$j],$lowerCaseApt);
                $j++;
            }
            $apt = trim($splitOnDirection[0]);            
        }
        
        return $jasper->process(
             base_path() . '/app/Reports/Lease.jrxml',
             base_path() .'/app/Documents/Leases/generated/lease_'.$this->id,
            array("pdf"),
            array("address_street" => '"'.$this->rentalUnit->address->street.'"',
                "address_apt" => '"'.$apt.'"',
                "address_city" => '"'.$this->rentalUnit->address->city.'"',
                "address_state" => '"'.$this->rentalUnit->address->state.'"',
                "address_zip" => '"'.$this->rentalUnit->address->zip.'"',
                "lease1"=>'"'.base_path() . '/app/Reports/lease1.png"',
                "lease2"=>'"'.base_path() . '/app/Reports/lease2.png"',
                "lease3"=>'"'.base_path() . '/app/Reports/lease3.png"',
                "lease4"=>'"'.base_path() . '/app/Reports/lease4.png"')
                    )->execute();
    }
}