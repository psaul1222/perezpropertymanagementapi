<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Appliance extends Model
{
    
    protected $table = 'appliances';
    public function applianceModel()
    {
        return $this->belongsTo("App\ApplianceModel");
    }
    public function leases()
    {
        return $this->belongsToMany("App\Lease","lease_appliance");
    }
    public function currentLease()
    {
        return $this->belongsToMany("App\Lease","lease_appliance")->where('end_date',null);;
    }
}
