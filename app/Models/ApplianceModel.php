<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ApplianceModel extends Model
{
    
    protected $table = 'appliance_models';
    public function applianceManufacturer()
    {
        return $this->belongsTo("App\ApplianceManufacturer");
    }
    public function applianceType()
    {
        return $this->belongsTo("App\ApplianceType");
    }
}
