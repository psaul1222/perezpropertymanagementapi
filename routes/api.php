<?php

use Illuminate\Http\Request;
use App\Http\Middleware\isAdmin;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('testing1',function(){
    return response([1,2,3,4],200);   
});
Route::post('auth', 'UserController@auth');

Route::group(['middleware' => ['auth:api','App\Http\Middleware\isAdmin']], function () {
    Route::resource('property', 'PropertyController',['only'=>['create','store','destroy']]);

    Route::resource('rentalUnit', 'RentalUnitController',['only'=>['create','store','destroy']]);

    Route::resource('lease', 'LeaseController',['only'=>['create','store','destroy']]);

    Route::resource('tenant', 'TenantController',['only'=>['create','store','destroy']]);
    
    Route::resource('payment', 'PaymentController',['only'=>['create','store','destroy']]);
    
    Route::resource('paymentType', 'PaymentTypeController',['only'=>['create','store','destroy']]);    
    
    Route::resource('user', 'UserController');
    Route::resource('applianceType', 'ApplianceTypeController');
    Route::resource('applianceModel', 'ApplianceModelController');
    Route::resource('applianceManufacturer', 'ApplianceManufacturerController');
    Route::resource('appliance', 'ApplianceController');
    Route::resource('setting', 'SystemSettingController');
    
});
Route::group(['middleware' => 'auth:api'], function () {
    
    Route::get('lease/{id}/getPdf', 'LeaseController@getPdf');
    Route::get('report/{apt_number}/getPdf', 'ReportController@getPdf');
    Route::post('report/generateRentalIncrease', 'ReportController@generateRentIncrease');
    
    Route::resource('property', 'PropertyController',['only'=>['index']]);
    
    Route::resource('rentalUnit', 'RentalUnitController',['only'=>['index']]);

    Route::resource('lease', 'LeaseController',['only'=>['index']]);

    Route::resource('tenant', 'TenantController',['only'=>['index']]);
    
    Route::put('payment/clear/{id}', 'PaymentController@clear');
    Route::resource('payment', 'PaymentController',['only'=>['index']]);
    
    Route::resource('paymentType', 'PaymentTypeController',['only'=>['index']]);
    
    Route::resource('access', 'AccessController',['only'=>['index']]);
    
});
